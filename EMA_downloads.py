import urllib.parse
import scrapy
from scrapy.http import Request
import utilities

class QuotesSpider(scrapy.Spider):
    name = "reports"
    start_urls = [
        'http://www.ema.europa.eu/ema/index.jsp?curl=pages%2Fmedicines%2Flanding%2Fepar_search.jsp&mid=WC0b01ac058001d124&searchTab=&alreadyLoaded=true&isNewQuery=true&status=Authorised&status=Withdrawn&status=Suspended&status=Refused&startLetter=View+all&keyword=Enter+keywords&searchType=name&taxonomyPath=&treeNumber=&searchGenericType=generics',
    ]
    allowed_domains = ["http://www.ema.europa.eu"]

    def parse(self, response):
        for link in response.xpath('//a[contains(@href, "medicines/human/medicines")]/@href').extract():
            yield Request(
                url=response.urljoin(link),
                callback=self.parse_pdf()
            )

        next_page = response.xpath("//li[@class='next last']/a[contains(text(),'next 25 results »')]/@href")
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def parse_pdf(self, response):
        for pdf in response.xpath('//a[contains(@href, "/docs/en_GB/document_library/EPAR_-_Product_Information")]/@href').extract():
            yield Request(
                url=response.urljoin(pdf),
                callback=self.save_pdf
            )

    def save_pdf(self, response):
        path = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)