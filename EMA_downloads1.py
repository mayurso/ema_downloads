import scrapy
import urllib.parse

from scrapy.http import Request


class ema(scrapy.Spider):
    name = "pdf_download"

    allowed_domains = ["www.ema.europa.eu"]
    start_urls = [
        "http://www.ema.europa.eu/ema/index.jsp?curl=pages%2Fmedicines%2Flanding%2Fepar_search.jsp&mid=WC0b01ac058001d124&searchTab=&alreadyLoaded=true&isNewQuery=true&status=Authorised&status=Withdrawn&status=Suspended&status=Refused&startLetter=View+all&keyword=Enter+keywords&searchType=name&taxonomyPath=&treeNumber=&searchGenericType=generics"]

    def parse(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "medicines/human/medicines" in href:
                print("-----target--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_article
                )

        next_page = response.xpath("//a[contains(text(),'next')]/@href").extract()
        next_page = "http://www.ema.europa.eu/ema/".join(next_page)
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def parse_article(self, response):
        for href in response.css('a[href$=".pdf"]::attr(href)').extract():
            print("***********PDF_________________", href)
            if "/docs/en_GB/document_library/EPAR_-_Product_Information" in href:
                yield Request(
                    url=response.urljoin(href),
                    callback=self.save_pdf
                )

    #         #response.css('a[href$=".pdf"]::attr(href)').extract()
    #         #for href in response.xpath('//a[contains(@href, "/docs/en_GB/document_library/EPAR_-_Product_Information")]/@href').extract():
    #            yield Request(
    #                url=response.urljoin(href),
    #                callback=self.save_pdf
    #            )

    def save_pdf(self, response):
        path = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)

