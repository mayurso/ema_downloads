import scrapy
from medmeme_spyder.utilities import s3_exists, get_s3_client, get_logger
import datetime
import os
from scrapy.http import Request

logger = get_logger('emaSpiderLog')

class emaSpider(scrapy.Spider):
    name = "emaSpider"

    # allowed_domains = ["www.ema.europa.eu"]
    # start_urls = [
    #     "http://www.ema.europa.eu/ema/index.jsp?curl=pages%2Fmedicines%2Flanding%2Fepar_search.jsp&mid=WC0b01ac058001d124&searchTab=&alreadyLoaded=true&isNewQuery=true&status=Authorised&status=Withdrawn&status=Suspended&status=Refused&startLetter=View+all&keyword=Enter+keywords&searchType=name&taxonomyPath=&treeNumber=&searchGenericType=generics"
    # ]

    def __init__(self):
        super(emaSpider, self).__init__()
        self.start_urls = [
            "http://www.ema.europa.eu/ema/index.jsp?curl=pages%2Fmedicines%2Flanding%2Fepar_search.jsp&mid=WC0b01ac058001d124&searchTab=&alreadyLoaded=true&isNewQuery=true&status=Authorised&status=Withdrawn&status=Suspended&status=Refused&startLetter=View+all&keyword=Enter+keywords&searchType=name&taxonomyPath=&treeNumber=&searchGenericType=generics"
        ]
        self.allowed_domains = ["www.ema.europa.eu"]
        self.s3_client = get_s3_client()
        self.bucket = 'mmcapturepoctemp'
        self.s3_root = os.path.join('data', datetime.datetime.now().strftime("%Y/%m/%d"), 'unstructured', 'raw', 'ema')
        self.upload_to_s3 = False

    def parse(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "medicines/human/medicines" in href:
                print("-----target--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_article
                )

        next_page = response.xpath("//a[contains(text(),'next')]/@href").extract()
        next_page = "http://www.ema.europa.eu/ema/".join(next_page)
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def parse_article(self, response):
        for href in response.css('a[href$=".pdf"]::attr(href)').extract():
            print("***********PDF_________________", href)
            if "/docs/en_GB/document_library/EPAR_-_Product_Information" in href:
                yield Request(
                    url=response.urljoin(href),
                    callback=self.save_pdf
                )

    def save_pdf(self, response):
        filename = response.url.replace('/', '~')
        s3_file_key = os.path.join(self.s3_root, filename)
        location = "/Users/bharatsamudrala/PycharmProjects/medmeme-scrapy-crawler/ema/"  # get_temp_download_directory()
        file_location = os.path.join(location, filename)
        if response.status == 200:

            if not s3_exists(self.s3_client, self.bucket, s3_file_key):
                self.upload_to_s3 = True

            with open(file_location, 'wb') as f:
                f.write(response.body)

            if self.upload_to_s3:
                self.s3_client.upload_file(
                    file_location,
                    self.bucket,
                    s3_file_key)
                logger.info('Moved{}toS3'.format(filename))
                # os.remove(filename)
                # logger.info(‘Removed {} from local’ .format(filename))
        else:
            logger.info('Not a 200  status - filename {}'.format(filename))
            logger.info('** ** ** STATUS CODE {} and url {}'.format(response.status, response.request.url))